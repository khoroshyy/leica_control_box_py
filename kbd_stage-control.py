# -*- coding: utf-8 -*-
"""
Created on Mon Jun 06 09:21:45 2016

@author: Petro Khoroshyy
@e-mail: khoroshyy@gmail.com

"""
#TODO  add stepsize notifier, may be wia system notification
# add always on top coordinates window, into one corner 
# 

import pyHook
import serial
import pythoncom
import time
from win32api import *
from win32gui import *
import win32con
import sys
import os
#import struct
import pywinauto

pwa_app = pywinauto.application.Application()
        
w_handle = pywinauto.findwindows.find_windows(title=u'OLYMPUS FLUOVIEW Ver.4.2b')[0]
window = pwa_app.window_(handle=w_handle)
window.SetFocus()
window.Minimize()
#time.sleep(10)
window.Maximize()
window.SetFocus()
pywinauto.win32functions.SetForegroundWindow(w_handle)
#
#print('End')

#pwa_app = pywinauto.application.Application()
#ctrl = window["AutoHV"]
#ctrl.ClickInput()
#ctrl = window['Button31']
#ctrl.ClickInput()
##w_handle = pywinauto.findwindows.find_windows(title=u'Coherent Connection', class_name='HwndWrapper[Coherent_Connection.exe;;3af3c594-066f-4f51-bfb6-acd6a54a3438]')[0]
#window = pwa_app.window_(handle=w_handle)
#window.SetFocus()
#w_handle = pywinauto.findwindows.find_windows(title=u'OLYMPUS FLUOVIEW Ver.4.2a', class_name='Afx:00400000:8:00010005:00000000:00050155')[0]
#window = pwa_app.window_(handle=w_handle)
#window.Maximize()
#window.SetFocus()
#ctrl = window['Button268']
#ctrl.ClickInput()
##ctrl = window['Button31']
#ctrl.Click()

## focusing down
ctrl = window['Button3']
#ctrl.ClickInput()
ctrl_up = window['Button4']
#ctrl_up.ClickInput()
ctrl_down = window['Button5']
#ctrl_down.ClickInput()
ctrldown = window['Button6']
#ctrldown.ClickInput()


#class WindowsBalloonTip:
#    def __init__(self, title, msg):
#        message_map = {
#                win32con.WM_DESTROY: self.OnDestroy,
#        }
#        # Register the Window class.
#        wc = WNDCLASS()
#        hinst = wc.hInstance = GetModuleHandle(None)
#        wc.lpszClassName = "PythonTaskbar"
#        wc.lpfnWndProc = message_map # could also specify a wndproc.
#        classAtom = RegisterClass(wc)
#        # Create the Window.
#        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
#        self.hwnd = CreateWindow( classAtom, "Taskbar", style, \
#                0, 0, win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, \
#                0, 0, hinst, None)
#        UpdateWindow(self.hwnd)
#        iconPathName = os.path.abspath(os.path.join( sys.path[0], "balloontip.ico" ))
#        icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
#        try:
#           hicon = LoadImage(hinst, iconPathName, \
#                    win32con.IMAGE_ICON, 0, 0, icon_flags)
#        except:
#          hicon = LoadIcon(0, win32con.IDI_APPLICATION)
#        flags = NIF_ICON | NIF_MESSAGE | NIF_TIP
#        nid = (self.hwnd, 0, flags, win32con.WM_USER+20, hicon, "tooltip")
#        Shell_NotifyIcon(NIM_ADD, nid)
#        Shell_NotifyIcon(NIM_MODIFY, \
#                         (self.hwnd, 0, NIF_INFO, win32con.WM_USER+20,\
#                          hicon, "Balloon  tooltip",msg,200,title))
#        # self.show_balloon(title, msg)
#        time.sleep(10)
#        DestroyWindow(self.hwnd)
#    def OnDestroy(self, hwnd, msg, wparam, lparam):
#        nid = (self.hwnd, 0)
#        Shell_NotifyIcon(NIM_DELETE, nid)
#        PostQuitMessage(0) # Terminate t444999999999111111111111777777777333333331111he app.

#def balloon_tip(title, msg):
#    w=WindowsBalloonTip(title, msg)

#if __name__ == '__main__':
    #balloon_tip("Title for popup", "This is the popup's message")



step = 50
step_d = round((step**2/2)**0.5)

ser = serial.Serial(9)  # open com port 10
ser.timeout = 1
ser.baudrate = 9600
print ser.portstr       # check which port was really used
#ser.write("?\r")      # write a string
#time.sleep(10)

#print ser.readline() 
#ser.close()             # close port
def OnKeyboardEvent(event):
    global step, step_d
    
    #print 'MessageName:',event.MessageName8
    #print 'Message:',event.Message
    #print 'Time:',event.Time
    #print 'Window:',event.Window
    #print 'WindowName:',event.WindowName
    #print 'Ascii:', event.Ascii, chr(event.Ascii)
    # print 'Key:', event.Key
    # print 'KeyID:', event.KeyID
    # print 'ScanCode:', event.ScanCode
    # print 'Extended:', event.Extended
    # print 'Injected:', event.Injected
    # print 'Alt', event.Alt
    #print 'Shift', event.Shift
    # print 'Transition', event.Transition
    # print '---'
    # print 'ScanCode:', event.ScanCode
    if event.Key == "Numpad7":
        #7
#       ser.write('GR' + str(int(step)) + ',' + str(int(step)) + '\r') 
        ser.write('GR -' + str(int(step)) + ',-' + str(int(step)) + '\r') 
        #print('GR ' + str(int(step_d)) + ',' + str(int(step_d)) + '\r') 
        #print 'ScanCode:', event.ScanCode
    elif event.Key == "Numpad8":
        #8
        #ser.write('GR ' + str(int(step)) + ',' + str(0) + '\r') 
        ser.write('GR -' + str(int(0)) + ',-' + str(int(step)) + '\r') 
        #print 'ScanCode:', event.ScanCode
        #print('GR ' + str(int(step)) + ',' + str(0) + '\r') 
    elif event.Key == "Numpad9":
        #9
        ser.write('GR ' + str(int(step_d)) + ',-' + str(step_d) + '\r') 
        #print 'ScanCode:', event.ScanCode
    elif event.Key == "Numpad4":
        #4
        ser.write('GR -' + str(int(step)) + ',' + str(0) + '\r') 
        #print 'ScanCode:', event.ScanCode
    elif event.Key == "Numpad6":
        #6
        ser.write('GR ' + str(int(step)) + ',' + str(0) + '\r') 
        #print 'ScanCode:', event.ScanCode
    elif event.Key == "Numpad1":
        #1
        ser.write('GR -' + str(int(step_d)) + ',' + str(int(step_d)) + '\r') 
        #print 'ScanCode:', event.ScanCode
    elif event.Key == "Numpad2":
        #2
        ser.write('GR -' + str(int(0)) + ',' + str(int(step)) + '\r') 
        #print 'ScanCode:', event.ScanCode
    elif event.Key == "Numpad3":
        #3
        ser.write('GR ' + str(int(step_d)) + ',' + str(int(step_d)) + '\r')
        #print 'ScanCode:', event.ScanCode
#    elif event.ScanCode == 76:
#        #5 go to zero789654123
#        ser.write('G 0,0\r')
        #print 'ScanCode:', event.ScanCode
        
#    elif event.KeyID == 107: #increase step
#        step = step * 5
#        step_d = int(round((step**2/2)**0.5))
#    elif event.KeyID == 109: # decrease step
#        step = step / 5
#        step_d = int(round((step**2/2)**0.5))        
# return True to pass the event to other handlers
    return True
#
def OnMouseEvent(event):
    # called when mouse events are received
    global ctrl_down, ctrl_up
    #coordinates2return = event.Position
    if event.Message == 522:
        #coordinates2return = event.Position
        if event.Wheel == -1:
            ctrl_down.Click()
        elif event.Wheel == 1:
            ctrl_up.Click()
    #SetCursorPos((coordinates2return[0],coordinates2return[1]))        
            
       # print 'MessageName:',event.MessageName
#        print 'Message:',event.Message
#        print 'Time:',event.Time
#        print 'Window:',event.Window
#        print 'WindowName:',event.WindowName
#        print 'Position:',event.Position
#        print 'Wheel:',event.Wheel
#        print 'Injected:',event.Injected
#        print '---'

# return True to pass the event to other handlers
    return True

# create a hook manager
hm = pyHook.HookManager()
# watch for all mouse events
hm.KeyDown = OnKeyboardEvent
# set the hook
hm.HookKeyboard()

# create a hook manager
#hm = pyHook.HookManager()
# watch for all mouse events
hm.MouseAll = OnMouseEvent
# set the hook
hm.HookMouse()
# wait forever


pythoncom.PumpMessages()