# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 19:09:58 2019

@author: OLYMPUS
"""
import win32gui

#import pyHook
import serial
import time

#from win32api import *
import win32api
from win32gui import *
import win32con
import time
#import struct
import pywinauto

MAIN_HWND = 0
hndl_list = []
def is_win_ok(hwnd, starttext):
    s = win32gui.GetWindowText(hwnd)
    if s.startswith(starttext):
            print(s)
            global MAIN_HWND
            MAIN_HWND = hwnd
            return None
    return 1


def find_main_window(starttxt):
    global MAIN_HWND
    win32gui.EnumChildWindows(0, is_win_ok, starttxt)
    return MAIN_HWND

def genlist(hwnd,lparam):
    global hndl_list
    print(hwnd)
    hndl_list.append(int(hwnd))
    return 1

def winfun(hwnd, lparam):
    global i
    global hndl_list
    s = win32gui.GetWindowText(hwnd)
#    if len(s) > 3:
#        print("winfun, child_hwnd: %d   txt: %s " % (hwnd, s))
#        #print('Internal ' + str(i))
#    
    print("winfun, child_hwnd: %d   txt: %s " % (hwnd, s))
    #hndl_list.append(hwnd)
    return 1

def get_identifiers():
    main_app = u'OLYMPUS FLUOVIEW Ver.4.2b'
    
    hwnd = win32gui.FindWindow(None, main_app)
    i = 1;
    print("Main handle")
    print(hwnd)
    if hwnd < 1:
        hwnd = find_main_window(main_app)
        print(hwnd)
        print("case one")
        i+1
    if hwnd:
        print("case two")
        #win32gui.EnumChildWindows(hwnd, winfun, None)
        win32gui.EnumChildWindows(hwnd, genlist, None)        
        
        i=i+1
    

get_identifiers()
up_focus_handle =  hndl_list[19]
down_focus_handle =  hndl_list[22]
zoom_handle = hndl_list[167]

rep_handle = hndl_list[383]
xy_scan_handle = hndl_list[387]
stop_handle = hndl_list[391]


pwa_app = pywinauto.application.Application().connect(title=u'OLYMPUS FLUOVIEW Ver.4.2b')
#app = Application.Connect(title="Software Title")        
w_handle = pywinauto.findwindows.find_windows(title=u'OLYMPUS FLUOVIEW Ver.4.2b')[0]
print(w_handle)
window = pwa_app.window_(handle=w_handle)
#window = pwa_app.window(handle=w_handle)
window.SetFocus()
window.Minimize()
#time.sleep(10).
window.Maximize()
window.SetFocus()
pywinauto.win32functions.SetForegroundWindow(w_handle)
#

#print(serial.__version__)
#ser = serial.Serial(port = 'COM4".
#                    baudrate = 57600,
#                    parity = serial.PARITY_NONE,
#                    stopbids = serial.STOPBITS_ONE
#                    )

#import MMCorePy
##load configuration
#mmc = MMCorePy.CMMCore()
#mmc.loadSystemConfiguration("C:\Program Files\Micro-Manager-1.4.21\IX83_alone.cfg")
#setup parameters for stage port
step = 25
step_d = round((step**2/2)**0.5)

ser = serial.Serial("COM10")  # open com port 10
ser.timeout = 10
ser.baudrate = 9600
#print(ser.portstr)      # check which port was really used



#setup serial port for Leica panel box
baudrate = 57600
#sol_com = '/dev/ttyUSB0'  # set the correct port before run it
sol_com = 'COM5'
comport = serial.Serial(port=sol_com, baudrate=baudrate)
comport.timeout = 20  # set read timeout
#comport.write("Lets start")
while True:
    ser_data = comport.read()
    print(hex(int.from_bytes(ser_data,byteorder='little')))
    if hex(int.from_bytes(ser_data,byteorder='little')) == "0xfb":
        comport.flushInput()
        comport.flushOutput()
        if (win32api.GetKeyState(win32con.VK_SHIFT)& 0x8000):
            ser.write(str.encode('GR -' + str(int(0)) + ',-' + str(int(step/10)) + '\r'))
        else:
            ser.write(str.encode('GR -' + str(int(0)) + ',-' + str(int(step)) + '\r'))
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xff":    
        #ser.write(str.encode('GR -' + str(int(0)) + ',' + str(int(step)) + '\r'))
        comport.flushInput()
        comport.flushOutput()
        if (win32api.GetKeyState(win32con.VK_SHIFT)& 0x8000):
            ser.write(str.encode('GR -' + str(int(0)) + ',' + str(int(step/10)) + '\r'))
        else:
            ser.write(str.encode('GR -' + str(int(0)) + ',' + str(int(step)) + '\r'))
            
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xe3":    
        ser.write(str.encode('GR -' + str(int(step)) + ',' + str(0) + '\r')) 
        comport.flushInput()
        comport.flushOutput()        
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xe7":    
        ser.write(str.encode('GR ' + str(int(step)) + ',' + str(0) + '\r'))
        comport.flushInput()
        comport.flushOutput()
#down left        
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xdb":    
        ser.write(str.encode('GR -' + str(int(step/2)) + ',' + str(int(-step/2)) + '\r'))
        comport.flushInput()
        comport.flushOutput()
#up right
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xdf":    
        ser.write(str.encode('GR ' + str(int(step/2)) + ',' + str(int(step/2)) + '\r'))
        comport.flushInput()
        comport.flushOutput()
#up left        
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xc3":    
        ser.write(str.encode('GR ' + str(int(step/2)) + ',' + str(int(-step/2)) + '\r'))
        comport.flushInput()
        comport.flushOutput()
#down right
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0xc7":    
        ser.write(str.encode('GR ' + str(int(-step/2)) + ',' + str(int(step/2)) + '\r'))
        comport.flushInput()
        comport.flushOutput()
            
        
    #focus control
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0x1b":    
        print("focus up")
        time.sleep(0.01)
        comport.flushInput()
        comport.flushOutput()
        window.child_window(handle=up_focus_handle).click()
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0x1f":    
        print("focus down")
        time.sleep(0.01)
        comport.flushInput()
        comport.flushOutput()
        window.child_window(handle=down_focus_handle).click()
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0x27":    
        print("zoom in")
        time.sleep(0.01)
        comport.flushInput()
        comport.flushOutput()
        window.child_window(handle=zoom_handle).scroll("up", "line")
        print(zoom_handle)
 #       window.child_window(handle=zoom_handle).click()
        window.child_window(handle=zoom_handle).set_focus()
        if (win32api.GetKeyState(win32con.VK_SHIFT)& 0x8000):
            pywinauto.mouse.scroll(coords=(0, 0), wheel_dist=1)
        else:
            pywinauto.mouse.scroll(coords=(0, 0), wheel_dist=10)
        
    elif hex(int.from_bytes(ser_data,byteorder='little')) == "0x23":    
        print("zoom out")
        print(zoom_handle)
        time.sleep(0.01)
        comport.flushInput()
        comport.flushOutput()
        #window.child_window(handle=zoom_handle).scroll("down", "page")
        window.child_window(handle=zoom_handle).set_focus()
        if (win32api.GetKeyState(win32con.VK_SHIFT)& 0x8000):
            pywinauto.mouse.scroll(coords=(0, 0), wheel_dist=-1)
        else:
            pywinauto.mouse.scroll(coords=(0, 0), wheel_dist=-10)
            
        #window.child_window(handle=zoom_handle).click()
        
        
#    if ser_data.rstrip() == "Start focusing":
#        # initiate focus search sequence.
#        print("Get current focus position")
#        # get curent focus position
#        # current_focus =  mmc.getPosition(mmc.getFocusDevice())
#        # print(current_focus)
#        print("And mooving up 100 um")
#        # focus_data_list = [] # create empty list to strore measured intensity
#        # create list for focus positions
#        print('list for focus data created')
#        # notify Solis that python is ready.
#        comport.write("Initial focusing position")
#    elif ser_data.rstrip() == "Focus point sent ":
#
#        focus_data_list.append( ser_data.rstrip()[17:] )
#        print(focus_data_list)
#        print("move focus")
#        print("start new focus point measurement")
#        z1serial.write("Point received, focus moved down ")
#    elif ser_data.rstrip() == "Calculate focus point":
#        print(focus_data_list)
#        print("calculated point")
#        z1serial.write("focus calculated")
#
#
#    elif ser_data.rstrip() == '':
#        print('timeout, empty string')
#    else:
#        print("unknown command")
#        time.sleep(0.1)
#
##mmc.setPosition(mmc.getFocusDevice(),1030)
#
##    else:
##        # print 'no data'
##        time.sleep(1)
