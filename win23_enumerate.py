# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 19:09:58 2019

@author: OLYMPUS
"""
import win32gui
MAIN_HWND = 0

def is_win_ok(hwnd, starttext):
    s = win32gui.GetWindowText(hwnd)
    if s.startswith(starttext):
            print(s)
            global MAIN_HWND
            MAIN_HWND = hwnd
            return None
    return 1


def find_main_window(starttxt):
    global MAIN_HWND
    win32gui.EnumChildWindows(0, is_win_ok, starttxt)
    return MAIN_HWND


def winfun(hwnd, lparam):
    global i
    s = win32gui.GetWindowText(hwnd)
    if len(s) > 3:
        print("winfun, child_hwnd: %d   txt: %s " % (hwnd, s))
        
    
    return 1

def main():
    main_app = u'OLYMPUS FLUOVIEW Ver.4.2b Viewer'
    hwnd = win32gui.FindWindow(None, main_app)
    i = 1;
    print(hwnd)
    if hwnd < 1:
        hwnd = find_main_window(main_app)
        print(hwnd)
        i+1
    if hwnd:
        win32gui.EnumChildWindows(hwnd, winfun, None)
        print(i)
        i=i+1
    

main()